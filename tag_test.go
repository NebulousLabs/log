package log

import "testing"

// TestTagManager is a simple unit test that covers all functionality of the Tag
// Manager.
func TestTagManager(t *testing.T) {
	t1 := "some-tag"
	t2 := "another-tag"

	tm := NewTagManager()
	if tm.HasTag(t1) || tm.HasTag(t2) {
		t.Fatal("unexpected")
	}
	tm.AddTag(t1)
	if !tm.HasTag(t1) {
		t.Fatal("unexpected")
	}
	tm.AddTag(t2)
	if !(tm.HasTag(t1) && tm.HasTag(t2)) {
		t.Fatal("unexpected")
	}
	tm.RemoveTag(t1)
	if tm.HasTag(t1) {
		t.Fatal("unexpected")
	}
	if !tm.HasTag(t2) {
		t.Fatal("unexpected")
	}
	tm.RemoveTag(t2)
	if tm.HasTag(t1) || tm.HasTag(t2) {
		t.Fatal("unexpected")
	}
}
